import { LitElement, html } from 'lit-element'

class PersonaSidebar extends LitElement {

	static get properties() {
		return {
			peopleStats: {type: Object},
			filterYIC: {type: Number},
			maxYearsInCompanyFilter: {type: Number}
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();
		this.peopleStats = {};
		this.filterYIC = 0;
		this.maxYearsInCompanyFilter;
	}


	render() {
		return html`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
			<aside>
				<section>
					<div>
						Hay <span class="badge-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
					</div>
					<div class="mt-5">
						<button class="w-100 btn bg-success" style="font-size: 50px" @click="${this.newPerson}">
							<strong>+</strong>
						</button>
					</div>
					<div class="mt-5">
						<label class="form-label">Filtro por a&ntilde;os en la empresa
						<input type="text" class="form-control" id="txtFilterYIC" value="" disabled></label>
						<input type="range" class="form-range" min="0" max="${this.maxYearsInCompanyFilter}" step="1" id="cusRngYearsComp"
						@change="${this.cusRngYearsCompChange}"
						.value="${this.filterYIC}">
					</div>
				</section>
			</aside>
		`;
	}

	newPerson(e) {
		console.log("newPerson en persona-sidebar")
		console.log("Se va a crear una persona");
		this.dispatchEvent(new CustomEvent("new-person", {}));
	}

	cusRngYearsCompChange(e) {
		console.log("cusRngYearsCompChange en persona-sidebar")
		this.filterYIC = this.shadowRoot.getElementById("cusRngYearsComp").value;
		this.shadowRoot.getElementById("txtFilterYIC").value = this.filterYIC;
		console.log("Se va a filtrar por anios en la empresa: " + this.filterYIC);
		this.dispatchEvent(new CustomEvent("filter-yic", {
			detail: {
				filterYIC: this.filterYIC
			}
		}));
	}
}

customElements.define('persona-sidebar', PersonaSidebar);