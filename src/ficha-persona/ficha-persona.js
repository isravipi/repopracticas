import { LitElement, html } from 'lit-element'

class FichaPersona extends LitElement {

	static get properties() {
		return {
			name: {type: String},
			yearsInCompany: {type: Number},
			personInfo: {type: String} //Vamos a hacer una propiedad calculada
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		//para llamar a la misma función de la misma clase. 
		super();

		//Asignamos valor
		this.name = "Prueba Nombre";
		this.yearsInCompany = 12;
	}

	//es parte del ciclo de vida de un componente litElement. Se llama despues de haber cambiado el valor de una propiedad
	//changedProperties es un map
	updated(changedProperties) {
		console.log("updated");
		changedProperties.forEach((oldValue, propName) => {
			console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue);
		});

		//para ver si ha cambiado una propiedad en concreto
		if (changedProperties.has("name")) {
			console.log("Propiedad name ha cambiado de valor, anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
		}

		if (changedProperties.has("yearsInCompany")) {
			console.log("Propiedad yearsInCompany ha cambiado de valor, anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
			this.updatePersonInfo();
		}
	}

	render() {
		return html`
			<div>
				<label>Nombre Completo</label>
				<!--<input text="text" id="fname" name "fname" value="${this.name}" @change="${this.updateName}"></input>-->
				<input text="text" id="fname" name "fname" value="${this.name}" @input="${this.updateName}"></input>
				<!--binding value="${this.name}", se podría poner tambien .value -->
				<!--actualizar con función manejadora que este en el class@change="${this.updateName}", nos hemos suscrito--> 
				<!--actualizar cada vez que cambia  con función manejadora que este en el class @input="${this.updateName}", nos hemos suscrito--> 
				<br />
				<label>Años en la empresa</label>
				<!--<input text="text" name "yearsInCompany" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}"></input>-->
				<input text="text" name "yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany};"></input>
				<br />
				<input text="text" value="${this.personInfo}"  disabled></input>
			</div>
		`;
	}

	/*updateName () {
		console.log("updateName");
	}

	updateYearsInCompany () {
		console.log("updateYearsInCompany");
	}*/

	//Si queremos ver el evento, y el elemento sobre el que se lanzado el evento. Asiganmos el valor de la propiedad.
	updateName (e) {
		console.log("updateName");
		this.name = e.target.value;
	}

	updateYearsInCompany (e) {
		console.log("updateYearsInCompany");
		this.yearsInCompany = e.target.value;
	}

	updatePersonInfo () {
		console.log("updatePersonInfo");

		if (this.yearsInCompany >= 7) {
			this.personInfo = "lead";
		} else if (this.yearsInCompany >=5) {
			this.personInfo = "senior";
		} else if (this.yearsInCompany >=3) {
			this.personInfo = "team";
		} else {
			this.personInfo = "junior";
		}		
	}		
}

customElements.define('ficha-persona', FichaPersona);