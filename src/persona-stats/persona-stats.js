import { LitElement, html } from 'lit-element'

class PersonaStats extends LitElement {

	static get properties() {
		return {
			people: {type: Array},
			maxYearsInCompany: {type:Number}
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();

		this.people = [];
	}


	updated(changedProperties) {
		console.log("updated en persona-stats");

		if(changedProperties.has("people")) {
			console.log("Ha cambiado la propiedad people en persona-stats");
			console.log("people.length: " +  this.people.length);
			let peopleStats = this.gatherPeopleArrayInfo(this.people);
			this.dispatchEvent(new CustomEvent("updated-people-stats", {
				detail: {
					peopleStats: peopleStats
				}
			}))
		}
	}

	gatherPeopleArrayInfo(people) {
		console.log("gatherPeopleArrayInfo");
		let peopleStats = {};
		peopleStats.numberOfPeople = people.length;
		console.log("gatherPeopleArrayInfo people.length: " +  people.length);
		let maxYearsInCompany = 0;

		people.forEach(person => {
			if (person.yearsInCompany > maxYearsInCompany) {
				maxYearsInCompany = person.yearsInCompany;
			}
		})

		console.log("gatherPeopleArrayInfo maxYearsInCompany: " +  maxYearsInCompany);
		peopleStats.maxYearsInCompany = maxYearsInCompany;

		return peopleStats;
	}


}

customElements.define('persona-stats', PersonaStats);