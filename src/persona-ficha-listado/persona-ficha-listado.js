import { LitElement, html } from 'lit-element'

class PersonaFichaListado extends LitElement {

	static get properties() {
		return {
			fname: {type: String},
			yearsInCompany: {type: Number},
			profile: {type: String},
			photo: {object: Object}
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();
	}

	render() {
		return html`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
			<div>
				<!--<label>Nombre</label>
				<input type="text" id="fname" value="${this.fname}" />
				<br />
				<label>A&ntilde;os en la empresa</label>
				<input type="text" name="yearsInCompany" value="${this.yearsInCompany}" />
				<br />
				<img src="${this.photo.src}" alt="${this.photo.alt}" width="200" height="200">
				<br />-->
				<div class="card h-100">
					<img class="card-img-top" src="${this.photo.src}" alt="${this.photo.alt}" width="125" height="150">
					<div class="card-body">
						<h5 class="card-title">${this.fname}</h5>
						<p class="card-text">${this.profile}</p>
						<ul class="list-group list-group-flush">
							<li class="list-group-item">${this.yearsInCompany} a&ntilde;os en la empresa</li>
						</ul>
					</div>
					<div class="card-footer">
						<button @click="${this.deletePerson}"class="btn btn-danger col-5"><strong>X</strong></button>
						<button @click="${this.moreInfo}"class="btn btn-info col-5 offset-1"><strong>Info</strong></button>
					</div>
			</div>
		`;
	}

	deletePerson(e){
		console.log("deletePerson en persona-ficha-listado")
		console.log("Se va a borrar la persona de nombre " + this.fname);
		this.dispatchEvent(
			new CustomEvent("delete-person", {
				detail: {
					name:this.fname
				}
			})
		);
	}

	moreInfo(e){
		console.log("moreInfo en persona-ficha-listado")
		console.log("Se ha pedido mas informacion de la persona " + this.fname);
		this.dispatchEvent(
			new CustomEvent("info-person", {
				detail: {
					name:this.fname
				}
			})
		);
	}
}

customElements.define('persona-ficha-listado', PersonaFichaListado);