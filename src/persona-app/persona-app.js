import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {

	static get properties() {
		return {
			people: {type: Array},
			filterYIC: {type: Number},
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();
	}

	render() {
		return html`
			<!--<h1>PersonaApp</h1>-->
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
			<persona-header></persona-header>
			<div class="row">
				<persona-sidebar @new-person="${this.newPerson}" @filter-yic="${this.filterPeopleYIC}" class="col-2"></persona-sidebar>
				<persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
			</div>
			<persona-footer></persona-footer>
			<persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
		`;	
	}

	updated(changedProperties) {
		console.log("updated");

		if(changedProperties.has("people")) {
			console.log("Ha cambiado la propiedad people en persona-app");
			this.shadowRoot.querySelector("persona-stats").people = this.people;
		}

		if(changedProperties.has("filterYIC")) {
			console.log("Ha cambiado la propiedad filterYIC en persona-app: " + this.filterYIC);
			this.shadowRoot.querySelector("persona-main").filterYIC = this.filterYIC;
		}
	}

	filterPeopleYIC(e) {
		console.log("filterPeopleYIC en persona-app");
		console.log(e.detail.filterYIC);
		this.filterYIC = e.detail.filterYIC;
	}

	newPerson(e) {
		console.log("newPerson en persona-app");
		this.shadowRoot.querySelector("persona-main").showPersonForm = true;
	}

	updatedPeople(e) {
		console.log("updatedPeople en persona-app");
		console.log(e.detail.people);
		this.people = e.detail.people;
	}

	updatedPeopleStats(e) {
		console.log("updatedPeopleStats en persona-app");
		console.log(e.detail);
		this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
		this.shadowRoot.querySelector("persona-sidebar").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;
	}
}

customElements.define('persona-app', PersonaApp);