import { LitElement, html } from 'lit-element'
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {
	static get properties() {
		return {
			people: {type: Array},
			showPersonForm: {type: Boolean},
			filterYIC: {type: Number}
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();
		this.people = [
			{
				name: "Milli y Vanilli",
				yearsInCompany: 10,
				profile: "Vulputate, lectus",
				photo: {
					src: "./img/ok2.gif",
					alt: "fotako ok2"
				}
			}, {
				name: "Juan de Dios",
				yearsInCompany: 2,
				profile: "Elementum eleifend augue hendrerit pretium.",
				photo: {
					src: "./img/ok5.gif",
					alt: "fotako ok5"
				}				
			}, {
				name: "Maricueca Mortimer",
				yearsInCompany: 5,
				profile: "Inceptos tortor ut parturient.",
				photo: {
					src: "./img/ok8.gif",
					alt: "fotako ok8"
				}
			}, {
				name: "Gordon Ramsey",
				yearsInCompany: 67,
				profile: "Potenti eleifend purus maecenas sociis.",
				photo: {
					src: "./img/ok3.gif",
					alt: "fotako ok3"
				}
			}, {
				name: "Maricuequer 2",
				yearsInCompany: 45,
				profile: "Vivamus ultricies tincidunt ligula.",
				photo: {
					src: "./img/ok4.gif",
					alt: "fotako ok4"
				}
			}
		];
		//this.peopleNoModified = this.people;
		this.showPersonForm=false;
		this.filterYIC=0;
	}

	render() {
		return html`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
			<h2 class="text-center">Personas</h2>
			<!--<main>
				${this.people.map(
					person => html`<persona-ficha-listado
						fname="${person.name}"
						yearsInCompany="${person.yearsInCompany}"
						.photo="${person.photo}" ></persona-ficha-listado>`
						//El punto en .photo es para poder pasarlo como objeto, si no, lo recibiria como texto, y no se escribe en el html, va "como" por dentro.
				)}
			</main>-->
			<div class="row" id="peopleList">
				<div class="row row-cols-1 row-cols-sm-4">
					${this.people.filter(
						person => person.yearsInCompany > this.filterYIC
					).map(
						person => html`<persona-ficha-listado
							fname="${person.name}"
							yearsInCompany="${person.yearsInCompany}"
							profile="${person.profile}"
							.photo="${person.photo}"
							@delete-person="${this.deletePerson}"
							@info-person="${this.infoPerson}"></persona-ficha-listado>`
							//El punto en .photo es para poder pasarlo como objeto, si no, lo recibiria como texto, y no se escribe en el html, va "como" por dentro.
					)}
				</div>
			</div>
			<div class="row">
				<persona-form id="personForm" class="d-none border rounder border-primary" @persona-form-close="${this.personFormClose}" @persona-form-store="${this.personFormStore}"></persona-form>

			</div>			
		`;
	}

	/*filterPeopleYIC() {
		console.log("filterPeopleYIC en persona-main");
		console.log("Se va a filtrar por anios en la empresa " + this.filterYIC);
		this.people = this.peopleNoModified.filter(
			person => person.yearsInCompany > this.filterYIC
		);
	}*/

	deletePerson(e) {
		console.log("deletePerson en persona-main");
		console.log("Se va a borrar la persona de nombre " + e.detail.name);

		this.people = this.people.filter(
			person => person.name != e.detail.name
		);
	}

	infoPerson(e) {
		console.log("infoPerson en persona-main");
		console.log("Se ha pedido mas informacion de la persona " + e.detail.name);

		let choosenPerson = this.people.filter(
			person => person.name === e.detail.name
		);

		let person = {};
		person.name = choosenPerson[0].name;
		person.profile = choosenPerson[0].profile;
		person.yearsInCompany = choosenPerson[0].yearsInCompany;

		this.shadowRoot.getElementById("personForm").person = person;
		this.shadowRoot.getElementById("personForm").editingPerson = true;
		this.showPersonForm = true;
	}

	updated(changedProperties) {
		console.log("updated");
		if (changedProperties.has("showPersonForm")) {
			console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
			if (this.showPersonForm === true){
				this.showPersonFormData();
			} else {
				this.showPersonList();
			}
		}

		if (changedProperties.has("people")) {
			console.log("Ha cambiado el valor de la propiedad people en persona-main");
			this.dispatchEvent(new CustomEvent("updated-people",{
				detail: {
					people: this.people
				}
			}));
		}

		/*if (changedProperties.has("filterYIC")) {
			console.log("Ha cambiado el valor de la propiedad filterYIC en persona-main: " + this.filterYIC);
			this.filterPeopleYIC();
		}*/
	}

	personFormClose() {
		console.log("personFormClose");
		console.log("Se ha cerrado el formulario de persona");
		this.showPersonForm = false;
	}

	personFormStore(e) {
		console.log("personFormStore");
		console.log("Se va a almacenar una persona");
		console.log("Name de la persona es " + e.detail.person.name);
		console.log("Name de la profile es " + e.detail.person.profile);
		console.log("Name de la yearsInCompany es " + e.detail.person.yearsInCompany);

		if (e.detail.editingPerson) {
			console.log("se va a actualizar la persona de nombre " + e.detail.person.name);
			/*let indexOfPerson = this.people.findIndex (
				person => person.name === e.detail.name
			);

			if (indexOfPerson >= 0) {
				console.log("Persona encontrada");
				this.people[indexOfPerson] = e.detail.person;
			}*/
			//Esta primera aproximación no genera un cambio en updated porque litElement no detecta que es un nuevo array
			this.people = this.people.map(
				person => person.name === e.detail.person.name ? person = e.detail.person : person);
		} else {
			console.log("Se va a guardar una persona nueva");
			//this.people.push(e.detail.person);
			this.people = [...this.people, e.detail.person];//Descompone el array en los elementos
		}
		
		console.log("Persona almacenada");
		this.showPersonForm = false;
	}

	showPersonList() {
		console.log("showPersonList");
		console.log("Mostrando formulario de personas");
		this.shadowRoot.getElementById("personForm").classList.add("d-none");
		this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
	}

	showPersonFormData() {
		console.log("showPersonFormData");
		console.log("Mostrando formulario de persona");
		this.shadowRoot.getElementById("personForm").classList.remove("d-none");
		this.shadowRoot.getElementById("peopleList").classList.add("d-none");
	}
}

customElements.define('persona-main', PersonaMain);