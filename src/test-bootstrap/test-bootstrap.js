import { LitElement, html, css } from 'lit-element'

class TestBootStrap extends LitElement {
	static get styles() {
		return css`
			.redbg {
				background-color: red;
			}

			.greenbg {
				background-color: green;
			}

			.bluebg {
				background-color: blue;
			}

			.greybg {
				background-color: grey;
			}
		`;
	}

	render() {
		return html`
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
			<h3>Test boostrap</h3>
			<h1>col- y col-sm</h1>
			<div class="row greybg">
				<div class="col-2 col-sm-6 redbg">Col 1</div>
				<div class="col-3 col-sm-1 greenbg">Col 2</div><!--col-sm-1 si es menor que 1 se aplica col-3. Probar reduciendo en diagonal la pantalla-->
				<div class="col-4 col-sm-1 bluebg">Col 3</div><!--col-sm-1 si es menor que 1 se aplica col-4. Probar reduciendo en diagonal la pantalla-->
			</div>
			<h1>offset</h1>
			<div class="row greybg">
				<div class="col-2 offset-1 redbg">Col 1</div>
				<div class="col-3 offset-2 greenbg">Col 2</div>
				<div class="col-4 bluebg">Col 3</div>
			</div>			
		`;
	}
}

customElements.define('test-bootstrap', TestBootStrap)