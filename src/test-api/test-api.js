import { LitElement, html } from 'lit-element'

class TestApi extends LitElement {
	static get properties() {
		return {
			movies: {type: Array}
		};
	}

	constructor() {
		super();
		this.movies = [];
		this.getMovieData();
	}

	render() {
		return html`
			${this.movies.map(
				movie => html`<div>La pel&iacute;cula <b>${movie.title}</b> fue dirigida por <b>${movie.director}</b></div>`
			)}
		`;
	}

	getMovieData() {
		console.log("getMovieData");
		console.log("obteniendo datos de las peliculas");

		let xhr= new XMLHttpRequest();
		xhr.onload = () => {
			if  (xhr.status === 200) {
				console.log("Petici�n completada correctamente");
				console.log(xhr);
				//console.log("response " + xhr.response);
				let APIResponse = JSON.parse(xhr.responseText);
				console.log("responseText: " + APIResponse);
				this.movies = APIResponse.results;
			}
		}

		xhr.open("GET", "https://swapi.dev/api/films/");
		xhr.send();
		console.log("Fin de getMovieData")
	}
}

customElements.define('test-api', TestApi)