import { LitElement, html } from 'lit-element'

class PersonaForm extends LitElement {

	static get properties() {
		return {
			person: {type: Object},
			editingPerson: {type: Boolean}
		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();
		this.resetFormData();
	}

	render() {
		return html`
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
		<div>
			<form>
				<div class="form-group">
					<label>Nombre Completo</label>
					<input type="text" class="form-control" placeholder="Nombre Completo"
					@input="${this.updateName}"
					.value="${this.person.name}" 
					?disabled="${this.editingPerson}"/>
				</div>
				<!--?disabled. Se pone el interrogante porque es un atributo de presencia-->
				<div class="form-group">
					<label>Perfil</label>
					<textarea class="form-control" placeholder="Perfil" rows="5"
					@input="${this.updateProfile}"
					.value="${this.person.profile}" /></textarea>
				</div>
				<div class="form-group">
					<label>A&ntilde;os en la empresa</label>
					<input type="text" class="form-control" placeholder="A&ntilde;os en la empresa"
					@input="${this.updateYearsInCompany}"
					.value="${this.person.yearsInCompany}" />
				</div>
				<button class="btn btn-primary" @click="${this.goBack}"><strong>Atr&aacute;s</strong></button>
				<button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>
			</form>
		</div>
		`;
	}

	updateName (e) {
		console.log("updateName");
		console.log("Actualizando la propiedad name con el valor " + e.target.value);
		this.person.name = e.target.value;
	}

	updateProfile (e) {
		console.log("updateProfile");
		console.log("Actualizando la propiedad profile con el valor " + e.target.value);
		this.person.profile = e.target.value;
	}

	updateYearsInCompany (e) {
		console.log("updateYearsInCompany");
		console.log("Actualizando la propiedad yearsInCompany con el valor " + e.target.value);
		this.person.yearsInCompany = e.target.value;
	}	

	goBack(e) {
		console.log("goBack");
		e.preventDefault();
		this.dispatchEvent(new CustomEvent("persona-form-close", {}));

		this.resetFormData();
	}

	storePerson(e) {
		console.log("storePerson");
		console.log("Se va a guardar una persona");
		e.preventDefault();
		//this.dispatchEvent(new CustomEvent("persona-form-close", {}));
		console.log("La propiedad name de la persona vale " + this.person.name);
		console.log("La propiedad profile de la persona vale " + this.person.profile);
		console.log("La propiedad yearsInCompany de la persona vale " + this.person.yearsInCompany);

		this.person.photo =  {
			src: "./img/ok1.gif",
			alt: "fotako ok1"
		};

		this.dispatchEvent(new CustomEvent("persona-form-store", {
			detail: {
				person:{
					name: this.person.name,
					profile: this.person.profile,
					yearsInCompany: this.person.yearsInCompany,
					photo: this.person.photo
				},
				editingPerson: this.editingPerson
			}
		}));
		
	}

	resetFormData() {
		console.log("resetFormData");
		this.person = {};
		this.person.name = "";
		this.person.profile = "";
		this.person.yearsInCompany = "";
		this.editingPerson = false;
	}
}

customElements.define('persona-form', PersonaForm);