import { LitElement, html } from 'lit-element'

class PersonaFooter extends LitElement {

	static get properties() {
		return {

		};
	}

	//Se puede definir un constructor para dar valor a las propiedades
	constructor() {
		super();

	}


	render() {
		return html`
			<h5>@PersonaApp 2021</h5>
		`;
	}


}

customElements.define('persona-footer', PersonaFooter);